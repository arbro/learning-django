from django.shortcuts import render
# from django.template.loader import get_template
from django.http import HttpResponse
import datetime
from selfprofile.forms import ContactForm
from django.http import HttpResponseRedirect
from django.core.mail import send_mail

from django.http.response import Http404


def hello(request):
    return HttpResponse("Hello World!")


def current_url_view(request):
    return HttpResponse("Welcome to the page at %s" % request.path)


def ua_display(request):
    try:
        ua = request.META['HTTP_USER_AGENT']
    except KeyError:
        ua = 'unknown'
    return HttpResponse('Your browser is %s' % ua)


def display_meta(request):
    values = request.META.items()
    values = sorted(values, key=lambda values: values[0], reverse=True)
    html = []
    for k, v in values:
        html.append('<tr><td>%s</td><td>%s</td></tr>' % (k, v))
    return HttpResponse('<table>%s</table>' % '\n'.join(html))


def current_datetime(request):
    now = datetime.datetime.now()
    # t = get_template('current_datetime.html')
    # html = t.render({'current_date': now})
    # return HttpResponse(html)
    return render(request, 'current_datetime.html', {'current_date': now})


def hours_ahead(request, offset):
    try:
        offset = int(offset)
    except ValueError:
        raise Http404()
    dt = datetime.datetime.now() + datetime.timedelta(hours=offset)
    html = "<html><body>In %s hour(s), it will be %s." \
           "</body></html>" % (offset, dt)
    return HttpResponse(html)


def contact(request):
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            send_mail(
                cd['subject'],
                cd['message'],
                cd.get('email', 'noreply@example.com'),
                ['siteowner@example.com'],
            )
            return HttpResponseRedirect('/contact/thanks/')
    else:
        form = ContactForm(
            initial={'subject': 'I love your site!'}
        )
    return render(request, 'contact_form.html', {'form': form})
